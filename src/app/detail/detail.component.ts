import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {

  private id :string;
  private isLoaded: boolean = false;
  private sub :Subscription;
  private user;
  private date;
  constructor(private activateRoute: ActivatedRoute,
              private dataService: DataService) { 
    this.sub = activateRoute.params.subscribe(params=>this.id=params['id']);
  }

  ngOnInit() {
    this.getUserInfo(this.id);
  }

  getUserInfo(id) {
    this.dataService.getUsers(id).subscribe(data => {
      this.user = data;
      this.isLoaded = true;
      this.date = Date.parse(data.registered.replace(/\s+/g, ''));
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
