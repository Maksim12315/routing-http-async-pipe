export class User {
    id: string;
    guid: string;
    registered: Date;
    isActive: boolean;
    balance: string;
    picture: string;
    age: number;
    eyeColor: string;
    name: string;
    gender: string;
    company: string;
    email: string;
    phone: string;
    address: string;
    about: string;
    latitude: number;
    longitude: number;
    tags: string[];
}