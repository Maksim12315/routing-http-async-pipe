import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  users: Observable<any>;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.users = this.dataService.getUsers();
  }

  deleteUser(id) {
    this.dataService.deleteUser(id).subscribe();
    window.location.reload();
  }

  onAddUser(user) {
    this.dataService.postUser('', user).subscribe();
    window.location.reload();
  } 

}
