import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  user: User = new User();
  @Output() onAddUser = new EventEmitter<User>();

  constructor() { }

  ngOnInit() {
  }

  addUser() {
    let a = +new Date();
    this.user.id = a.toString(36);
    this.user.picture = 'http://placehold.it/32x32';
    this.user.registered = new Date();
    +this.user.age;
    this.onAddUser.emit(this.user);
  }

}
