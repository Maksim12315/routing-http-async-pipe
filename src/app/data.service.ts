import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private url = 'http://localhost:3000/users/';

  constructor( private http: HttpClient) { }

  getUsers(url: string = ''): Observable<any>{
    return this.http.get(this.url + url);
  }

  deleteUser(id): Observable <any>{
    return this.http.delete(this.url +id);
  }
    
  postUser(url: string = '', data: any = {}): Observable<any> {
    return this.http.post(this.url + url, data);
  }
}
